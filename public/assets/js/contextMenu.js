$(document).bind("contextmenu", function(event) {
	event.preventDefault();
});

$(document).click(function(evt){
	let div = $(event.target).attr('class');
	if(div == "menu-more-trigger" || div == "fas fa-ellipsis-h")
		return;

	isHovered = $("ul.contextMenu").is(":hover");
	$("ul.contextMenu").empty();

});

function contextMenuGenerator(options){
	_this = this;

	let defaultOptions = {
		container	: ".contextMenu",
		menu		: [],
		roleMenu	: [],
		data		: [],
		login_id	: null,
	}

	options = { ...defaultOptions, ...options };
	console.log(options)
	this.init = function(){

		// RIGHT CLICK MENU
		$('.file-box').bind("contextmenu", function(event) {
			// empty the box
			html	 = '';
			$("ul.contextMenu").empty();

			// get data ID
			let _id 	= $(this).attr('id');
			let _data	= options.data[_id]

			html 		+= _this.commonMenu(_id, _data);
			html 		+= _this.specialMenu(_id, _data);

			// append to HTML
			$(options.container).append(html);

			$('.customization_popup_trigger').on('click', function(event) {
				event.preventDefault();
				_this.renderDetail(_data);
				$('.customization_popup').addClass('is-visible');
			});

			$("ul.contextMenu")
				.show()
				.css({top: event.pageY + 15, left: event.pageX + 10});
		});

		// menu more
		$(".file-box").on("click", "a", function (event) {

			// empty the box
			html	 = '';
			$("ul.contextMenu").empty();

			// get clicked data ID
			let _id = $(this).attr('id').split("-");
			_id = _id[2];
			let _data	= options.data[_id]

			html 		+= _this.commonMenu(_id, _data);
			html 		+= _this.specialMenu(_id, _data);

			// append to HTML
			$(options.container).append(html);

			$('.customization_popup_trigger').on('click', function(event) {
				event.preventDefault();
				_this.renderDetail(_id, _data);
				$('.customization_popup').addClass('is-visible');
			});

			$("ul.contextMenu")
				.show()
				.css({top: event.pageY + 15, left: event.pageX + 10});
		});
	}

	this.renderDetail = function(_id, data){

		// empty the box
		$('.tab').empty();
		$('#details_title').empty();
		$('#activity_tab').empty()

		let detail 		= '';
		let activity 	= '';

		var title		= data.name;
		var buttons 	= '<div class="review-btn"></div>';
		var type 		= '<tr><th>Type</th><td>'+this.url_ext(data.doc_file)+'</td></tr>';
		var owner 		= '<tr><th>Owner</th><td>'+data.user_owner+'</td></tr>';
		var created 	= '<tr><th>Created</th><td>'+this.formatDate(data.entry_date)+'</td></tr>';
		var tags		= '<tr><th>Tags</th><td>'+this.formatTags(data.tags)+'</td></tr><tr><th>Detail</th></tr>';
		var description	= '<description>'+data.description+'</description>';

		var status		= (data.is_public == 1)? 'anyone can access' : 'shared with spesific people';

		detail += buttons;
		detail += '<table>';
		detail += type;
		detail += owner;
		detail += created;
		detail += tags;
		detail += '</table>';
		detail += description;

		if(data.is_public == 1 && data.next_approval == null){
			detail += '<span class="alert"><select name="is_public" id="is_public_opt"><option value=1>Dokumen Publik</option><option value=0>Dokumen Terbatas</option></select></span>';
		}else{
			detail += '<span class="alert">*'+status+'</span>';
		}

		$.each( data.status, function( key, value ) {
			activity += _this.activityList(value);
		});

		activity += this.activityList(data);
		activity += '<div> <span class="alert">*no recorded activity before '+this.formatDate(data.entry_date)+'.</span></div>';

		$('#details_title').append(title);
		$('#details_tab').append(detail);
		$('#activity_tab').append(activity);

		// Review button if waiting for user approval
		console.log(data)
		if(options.login_role_id == data.next_approval){
			var _btn = '<a href="#" data-toggle="modal" data-target="#formModal" class="btn btn-info btn-icon-split"><span class="icon text-white-50"><i class="fas fa-info-circle"></i></span><span class="text">Review</span></a>';
			$('.review-btn').append(_btn)

			if(options.form)
				_this.popupMenu(_id, options.form);
		}
	}

	this.commonMenu = function(_id, _data){
		html = '';
		$.each( options.menu, function( key, value ) {
			url = (value[2] == 'Preview') ? _data.doc_file : value[0]+'/'+_id;
			html += _this.menu(url,value[1],value[2], value[3]);
		});

		return html;
	}

	this.specialMenu = function(_id, _data){
		html = '<hr>';

		$.each( options.roleMenu, function( key, value ) {
			var url = value[0]+'/'+_id;

			if(value[2] == 'Approve Document'){
				// if(options.login_role_id == _data.next_approval)
				// 	html += _this.menu(url,value[1],value[2], value[3]);
				return;

			}else if(options.login_id == _data.id_user){
				if(value[2] == 'Remove Document'){
					html += _this.menu(url,value[1],value[2], value[3]);
					_this.popup(url, 'Remove Document', 'Are you sure you want to permanently remove the document?', 'Remove');

				// }if(value[2] == 'Edit Document'){
					// html += _this.menu(url,value[1],value[2], value[3]);
				}else{
					html += _this.menu(url,value[1], value[2], value[3]);
				}
			}


		});


		return html;
	}

	this.formatTags = function(tags){
		var tag 	= tags.split(",");
		let html	= '';

		$.each( tag, function( key, value ) {
			html += '<a href="'+base_url+'/doc/category?q='+value+'" class="tag-label">'+value+'</a>';
		});

		return html;
	}

	this.formatDate = function(d){
		var d 		= new Date(d);
		var months 	= ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		d 			= d.getDate()+" "+months[d.getMonth()]+" "+d.getFullYear();
		
		return d;
	}

	this.menu = function(url = '#', icon, text, additional=''){
		html = '<li><a href="'+url+'" '+additional+'><i class="'+icon+'"></i> '+text+'</a></li>';
		return html;
	}

	this.popup = function(url = '', title = '', message = '', button = 'ok'){
		$('#removeModal').remove();
		popup = '<div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">'+title+'</h5> <button class="close" type="button" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button></div><div class="modal-body">'+message+'</div><div class="modal-footer"> <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button> <a class="btn btn-primary" href="'+url+'">'+button+'</a></div></div></div></div>';
		$('body').append(popup);
		return true;
	}

	this.popupMenu = function(_id, form){
		console.log(form, _id)
		$('#formModal').remove();

		var fields = _this.renderField(_id, form.fields);

		var popup = '<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog-centered modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">'+form.title+'</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button></div><div class="modal-body">'+fields+'</div><div class="modal-footer">'+form.button+'</div></div></div></div>';
		$('body').append(popup);
		return true;
	}

	this.renderField = function(_id, fields){
		let html = '';

		html = '<div class="row justify-content-center align-self-center">';
		$.each( fields, function( key, value ) {
			html += _this.fieldType(_id, value);
		});
		html += '</div>'

		return html;
	}

	this.fieldType = function(_id, field){
		let html = '';

		switch (field.type) {
			case 'bigdiv':
				html += '<a href="'+field.url+_id+'" class="revisi-menu col-lg-4">';
					html += '<div class="revisi-menu-icon" style="background: center no-repeat url('+base_url+field.icon+'); background-size: 100px;">';
					html += '</div>';
					html += '<div class="revisi-menu-label">';
						html += '<h3>'+field.label+'</h3>';
						html += '<p>'+field.description+'</p>';
					html += '</div>';
				html += '</a>';
				break;
			case 'text':
				html = 'Microsoft Excel file';
				break;
			default:
				html = '';
				break;
		}

		return html;
	}

	this.activityList = function(data){
		let activity 	= '';
		let status 		= null;
		let entry_date	= _this.formatDate(data.entry_date);

		switch (data.description) {
			case 'approve':
				status = 'menyetujui dokumen';
				break;
			case 'update_ver':
				status = 'memperbaiki dokumen sebelumnya';
				break;
			case 'reject':
				status = 'menolak dokumen';
				break;
			case 'edit':
				status = 'mengubah dokumen';
				break;

			default:
				status = 'mengunggah dokumen';
				break;
		}

		activity += '<div class="activity_list">';
			activity += '<div class="activity_list_date">'+entry_date+'</div>';
			activity += '<div class="activity_list_detail"><span>'+data.name+'</span> '+status+'</div>';
			if(data.detail)
				activity += '<div class="activity_list_detail desc">'+data.detail+'</div>';

			if(data.doc_file)
				activity += '<div class="activity_list_detail link"><a target="_blank" href="'+data.doc_file+'">document</a></div>';
			
			
		activity += '</div>';

		return activity;
	}

	this.url_ext = function(url){
		var ext 	= url.split(/[#?]/)[0].split('.').pop().trim();
		let _ext	= null;

		switch (ext) {
			case 'css':
				_ext = 'Cascading Style Sheet file';
				break;
			case 'xls':
				_ext = 'Microsoft Excel file';
				break;
			case 'xlsx':
				_ext = 'Microsoft Excel Open XML spreadsheet file';
				break;
			case 'ppt':
				_ext = 'PowerPoint  presentation';
				break;
			case 'pptx':
				_ext = 'PowerPoint Open XML presentation';
				break;
			case 'doc':
				_ext = 'Microsoft Word File';
				break;
			case 'docx':
				_ext = 'Microsoft Word File';
				break;
			case 'pdf':
				_ext = 'PDF File';
				break;
			case 'txt':
				_ext = 'Plain text file';
				break;
			default:
				_ext = ext;
				break;
		}
		return _ext;
	}

	this.init();

}