<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class UserModel extends Model
{
	protected $table			= 'ms_user';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['name', 'position', 'email', 'id_role', 'entry_stamp'];
	protected $session;


	public function __construct()
	{
		$this->session 	= session();
	}

	public function get_data($id = null)
	{
		if ($id !== null) {
			$data = $this
				->select('ms_user.*, tb_roles.name role')
				->join('tb_roles', 'tb_roles.id = ms_user.id_role', 'LEFT')
				->where('ms_user.id', $id)
				->get()->getRow();
			return $data;
		} else {
			$data = $this
				->select('ms_user.*, tb_roles.name role')
				->join('tb_roles', 'tb_roles.id = ms_user.id_role', 'LEFT')
				->where('ms_user.is_active', 1)
				->get()->getResult();
			return $data;
		}
		// select a.*,b.name role FROM ms_user a LEFT JOIN tb_roles b ON b.id = a.id_role
	}

	public function create($data = null)
	{
		//saving data
		$now 				= date("Y-m-d H:i:s");
		$data['entry_date'] = $now;
		$pw 				= password_hash($data['password'], PASSWORD_BCRYPT);
		unset($data['password']);

		// $save		= $this->savedb('ms_user', $data);
		$save		= $this->save($data);
		$id_user 	= $this->insertID();
		$this->create_user_username($id_user, $data['email']);
		$this->create_user_password($id_user, $pw);

		return TRUE;
	}

	public function create_user_username($id_user = '', $username = '')
	{
		$payload = array(
			'id_user' => $id_user,
			'username' => $username,
			'entry_stamp' => date("Y-m-d H:i:s"),
			'is_active' => 1,
		);

		$this->savedb('tr_username', $payload);
	}

	public function create_user_password($id_user = '', $password = ''){
		$payload = array(
			'id_user' => $id_user,
			'password' => $password,
			'entry_stamp' => date("Y-m-d H:i:s"),
			'is_active' => 1,
		);

		$this->savedb('tr_password', $payload);
	}

	public function check_login($_param){

		$user = $this
			->select('ms_user.*, tb_role.name role')
			->join('tb_role', 'tb_role.id = ms_user.id_role')
			->where('username', $_param['username'])
			->where('ms_user.is_active', 1)
			->get()->getRow();
			
		if ($user->id !== null) {
			return $user;
		} else {
			return FALSE;
		}
	}

	public function update_user_username($data_)
	{
		$payload['id_user'] = $data_['id_user'];
		$payload['username'] = $data_['username'];
		$payload['is_active'] = $data_['is_active'];
		// print_r($payload);die;
		$this->updatedb('tr_username', $payload, $payload['id_user']);
	}

	private function savedb($table, $payload)
	{
		echo "save".$table;
		$db 		= db_connect('default');
		$builder 	= $db->table($table);
		$builder->insert($payload);
		// print_r($this->getLastQuery());
		
	}

	private function updatedb($table, $payload, $id_user)
	{
		$db 		= db_connect('default');
		$builder 	= $db->table('tr_username');
		$builder->where('id_user', $id_user);
		$builder->update($payload);
		// return $data;
		// print_r($builder->update($payload));die;
		// print_r($this->getLastQuery());
	}
}
