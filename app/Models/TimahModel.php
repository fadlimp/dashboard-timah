<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class TimahModel extends Model
{
	protected $table			= 'ms_timah';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['stored_at', 'quality', 'quantity', 'price', 'total_price', 'date', 'is_sold', 'quantity_sold', 'description', 'entry_stamp', 'edit_stamp'];
	protected $session;


	public function __construct()
	{
		$this->session 	= session();
	}

	public function get_data($user = null)
	{
		if ($user !== null) {
			$data = $this
				->select('ms_timah.*')
				->where('ms_timah.description', $user)
				->orderBy('id', 'desc')
				->get()->getResult();
			return $data;
		} else {

			$data = $this
				->select('ms_timah.*')
				->orderBy('id', 'desc')
				->get()->getResult();

			return $data;
		}
	}
	public function get_data_transaction($user = null)
	{
		if ($user !== null) {
			$data = $this
				->select('ms_timah_transaction.*')
				->orderBy('id', 'desc')
				->get()->getResult();
			return $data;
		} else {
			$db 					= db_connect('default');
			$builder 				= $db->table('ms_timah_transaction');

			$data = $builder
				->select('*')
				->get()->getResult();

			return $data;
		}
	}
	public function getAvailableData()
	{
		$data = $this
			->select('*')
			->where('quantity_sold', '!= ms_timah.quantity')
			->orderBy('id', 'desc')
			->get()->getResult();

		$options = [];
		foreach ($data as $key => $value) {
			# code...
			$options[$value->id] = $value->description . ' - OC ' . $value->quality . ' - sisa stock ' . $value->quantity . "Kg";
		}
		// print_r($this->getLastQuery());

		return $options;
	}


	public function create($data = null)
	{
		//saving data
		// $now 				= date("Y-m-d H:i:s");
		// $data['entry_date'] = $now;

		$save		= $this->savedb('ms_timah_transaction', $data);
		// print_r($this->getLastQuery());
		return TRUE;
	}

	private function savedb($table, $payload)
	{
		$db 					= db_connect('default');
		$builder 				= $db->table($table);
		$builder->insert($payload);

		return TRUE;
	}

	private function updatedb($table, $payload, $id_user)
	{
		$db 		= db_connect('default');
		$builder 	= $db->table('tr_username');
		$builder->where('id_user', $id_user);
		$builder->update($payload);
	}
}
