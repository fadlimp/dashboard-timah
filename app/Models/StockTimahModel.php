<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class StockTimahModel extends Model
{
	protected $table			= 'ms_timah';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['date', 'quality', 'quantity', 'price', 'total_price', 'stored_at', 'description', 'date', 'entry_stamp'];
	protected $session;

	public function __construct()
	{
		$this->session 	= session();
	}

	public function get_data($user = null)
	{
		if ($user !== null) {
			$data = $this
				->select('ms_timah.*')
				->where('ms_timah.description', $user)
				->orderBy('id', 'desc')
				->get()->getResult();
			return $data;
		} else {
			$data = $this
				->select('ms_timah.*')
				->orderBy('id', 'desc')
				->get()->getResult();

			// print_r($this->getLastQuery());
			return $data;
		}
	}

	function getDataOc()
	{

		$data['good'] = $this
			->select('sum(quantity) as total')
			->where('quality > 60')
			->get()->getRow();
		$data['bad'] = $this
			->select('sum(quantity) as total')
			->where('quality <= 60')
			->get()->getRow();

		// print_r($this->getLastQuery());
		return $data;
	}
	private function savedb($table, $payload)
	{
		$db 					= db_connect('default');
		$builder 				= $db->table($table);
		$now 					= date("Y-m-d H:i:s");
		$payload['entry_date'] 	= $now;
		$builder->insert($payload);

		return TRUE;
	}

	private function updatedb($table, $payload, $id_user)
	{
		$db 		= db_connect('default');
		$builder 	= $db->table('tr_username');
		$builder->where('id_user', $id_user);
		$builder->update($payload);
	}
}
