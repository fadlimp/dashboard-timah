<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class NotificationModel extends Model
{
	protected $table			= 'tr_notification';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['id_user','id_document','status','description','entry_date','delete'];
	public $db;


	public function __construct(){
		$this->db				= db_connect('default'); 
	}

	public function get_data($id_user = null){
		if($id_user !== null){
			$data = $this->where(['id_user' => $id_user])->get()->getRow();
			return $data;
		}else{
			return FALSE;
		}
	}

	public function create($payload = null, $id_role = null){
		$payload['status'] 		= 0;
		$payload['entry_date'] 	= date("Y-m-d H:i:s");
		$payload['delete'] 		= 0;

		// SAVE DOCUMENT
		$save = $this->save($payload);
		return TRUE;
	}

	public function get_users($id_role){
		$users	= $this->db->where('id_role', $id_role)
							->where('delete', 0)
							->get()
							->getResult();
		return $users;
	}


}
