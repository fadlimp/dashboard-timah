<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class StoreModel extends Model
{
	protected $table			= 'store_transaction';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['id_user', 'id_store', 'amount', 'discount', 'description', 'date', 'is_cash', 'is_paid', 'entry_stamp'];
	protected $session;


	public function __construct()
	{
		$this->session 	= session();
	}

	public function get_data($user = null)
	{
		if ($user !== null) {
			$data = $this
				->select('store_transaction.*')
				->orderBy('id', 'desc')
				->get()->getResult();

			return $data;
		} else {
			$data = $this
				->select('store_transaction.*, tb_store.name toko')
				->join('tb_store', 'tb_store.id = store_transaction.id_store', 'left')
				->orderBy('store_transaction.id', 'desc')
				->get()->getResult();

			return $data;
		}
	}

	function getDataSummary()
	{

		$data = $this
			->select('YEAR(store_transaction.date) as year, MONTH(store_transaction.date) as month, sum(store_transaction.amount) as total')
			->groupBy(['YEAR(store_transaction.date)', 'MONTH(store_transaction.date)'])
			// ->groupBy('MONTH(date)')
			->get()->getResult();

		return $data;
	}

	public function create($data = null)
	{
		//saving data
		$now 				= date("Y-m-d H:i:s");
		$data['entry_date'] = $now;

		$save		= $this->save($data);

		return TRUE;
	}

	private function savedb($table, $payload)
	{
		$db 					= db_connect('default');
		$builder 				= $db->table($table);
		$now 					= date("Y-m-d H:i:s");
		$payload['entry_date'] 	= $now;
		$builder->insert($payload);

		return TRUE;
	}

	private function updatedb($table, $payload, $id_user)
	{
		$db 		= db_connect('default');
		$builder 	= $db->table('tr_username');
		$builder->where('id_user', $id_user);
		$builder->update($payload);
	}
}
