<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class LoanModel extends Model
{
	protected $table			= 'tr_debts';
	protected $primaryKey		= 'id';
	protected $allowedFields	= ['id_user', 'type', 'total', 'description', 'date', 'entry_stamp'];
	protected $session;


	public function __construct()
	{
		$this->session 	= session();
	}

	public function get_data($user = null)
	{
		if ($user !== null) {
			$data = $this
				->select('tr_debts.*')
				->where('tr_debts.description', $user)
				->orderBy('id', 'desc')
				->get()->getResult();
			return $data;
		} else {
			$data = $this
					->select('tr_debts.id, tr_debts.description, tr_debts.type, tr_debts.date, sum(total) as total')
					->groupBy('tr_debts.description')
					->orderBy('id', 'desc')
					->get()->getResult();

				// print_r($this->getLastQuery());
			return $data;
		}
		
	}


	public function getTotalLoan($user = null)
	{
		
		if ($user !== null) {
			$loan = $this
					->selectSum('total')
					->where('tr_debts.description', $user)
					->where('type', 'out')
					// ->groupBy('tr_debts.description')
					->get()->getRow();
			$pay = $this
					->selectSum('total')
					->where('tr_debts.description', $user)
					->where('type', 'in')
					// ->groupBy('tr_debts.description')
					->get()->getRow();
			$data = $loan->total - $pay->total;
			return $data;
		} else {
			$loan = $this
					->selectSum('total')
					->where('type', 'out')
					->get()->getRow();
			$pay = $this
					->selectSum('total')
					->where('type', 'in')
					->get()->getRow();
			$data = $loan->total - $pay->total;
			return $data;
		}
			
		
	}

	public function create($data = null)
	{
		//saving data
		$now 				= date("Y-m-d H:i:s");
		$data['entry_date'] = $now;
		$pw 				= password_hash($data['password'], PASSWORD_BCRYPT);
		unset($data['password']);

		// $save		= $this->savedb('ms_user', $data);
		$save		= $this->save($data);
		$id_user 	= $this->insertID();
		$this->create_user_username($id_user, $data['email']);
		$this->create_user_password($id_user, $pw);

		return TRUE;
	}

	private function savedb($table, $payload)
	{
		$db 					= db_connect('default');
		$builder 				= $db->table($table);
		$now 					= date("Y-m-d H:i:s");
		$payload['entry_date'] 	= $now;
		$builder->insert($payload);
		
		return TRUE;
		
	}

	private function updatedb($table, $payload, $id_user)
	{
		$db 		= db_connect('default');
		$builder 	= $db->table('tr_username');
		$builder->where('id_user', $id_user);
		$builder->update($payload);
	}
}
