<?php

namespace App\Controllers\Stock;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\Request;
use App\Models\TimahModel;

helper('number');

class Timah extends BaseController
{
	public $tm;
	public $generateTable;
	public $filters = [
		'local_number'	=> '\CodeIgniter\View\Filters::local_number',
		'capitalize'	=> '\CodeIgniter\View\Filters::capitalize',
	];


	public function __construct()
	{
		$this->tm 		= new TimahModel();
		$this->generateTable    = new \CodeIgniter\View\Table();
	}

	public function index()
	{
		setlocale(LC_MONETARY, 'en_US');

		$data['data']   	= $this->tm->get_data();
		// print_r($data);
		// die;
		if (count($data['data']) > 0) {
			foreach ($data['data'] as $key => $value) {
				$quality = $value->quality > 60 ? 'Tinggi' : 'Rendah';
				$data['table'][$key] = array($value->description, $value->date, $quality . " (" . $value->quality . ")", number_format($value->total_price, 2, ',', '.'), number_format($value->quantity, 2, ',', '.'),  number_format($value->quantity_sold, 2, ',', '.'));
			}

			$data['table']  = $this->generateTable->generate($data['table']);
		}
		// print_r($data);
		return view('stock/timah/list', $data);
	}

	public function detail()
	{
		setlocale(LC_MONETARY, 'en_US');

		$data['data']   	= $this->tm->get_data($_GET['name']);
		$data['piutang']	= $this->tm->getTotalLoan($_GET['name']);

		if (count($data) > 0) {
			foreach ($data['data'] as $key => $value) {
				$color = ($value->type == 'in') ? 'green' : 'red';
				$icon = ($value->type == 'in') ? '<i class="fas fa-angle-down"></i>' : '<i class="fas fa-angle-up"></i>';
				$data['table'][$key] = array($value->description, date_format(date_create($value->date), "d M Y"), ($value->type == 'in') ? 'Uang Masuk' : 'Uang Keluar', '<p style="color:' . $color . '">' . $icon . '&nbsp;Rp. ' . $value->total . '</p>');
			}
			// <div class="mt-4 mb-2">
			// 	<a href="'.base_url('/admin/master/user/edit/'.$value->id) . '" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-pen text-white"></i> </a>
			// 	<a href="'.base_url('/admin/master/user/delete/'.$value->id) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
			// </div>

			$data['table']  = $this->generateTable->generate($data['table']);
		}

		return view('loan/list', $data);
	}

	public function insert()
	{

		if (($_POST)) {
			# code...
			print_r($_POST);
			$data = $this->tm->save($_POST);

			if ($data)
				return redirect()->to(base_url('stock/timah'));
		}

		return view('stock/timah/form_input');
	}

	public function transaction()
	{

		$data = [];
		$data['list_timah'] = $this->tm->getAvailableData();
		if (($_POST)) {
			# code...
			$payload = [
				'type' => 'out',
				'date' => $_POST['date'],
				'quality_avg' => $_POST['quality_avg'],
				'amount' => $_POST['amount'],
				'quantity' => $_POST['quantity_total']
			];
			// print_r($_POST);
			// print_r($payload);
			$data = $this->tm->create($payload);
			// die;
			if ($data)
				return redirect()->to(base_url('stock/timah/list_transaction'));
		}

		return view('stock/timah/form_transaction', $data);
	}

	public function list_transaction()
	{

		$data['data']   	= $this->tm->get_data_transaction();
		// print_r($data['data']);
		// die;
		if (count($data['data']) > 0) {
			foreach ($data['data'] as $key => $value) {
				$quality = $value->quality_avg > 60 ? 'Tinggi' : 'Rendah';
				$data['table'][$key] = array($value->date, $value->quantity, $quality . " (" . $value->quality_avg . ")", "Rp. " . number_format($value->amount, 2, ',', '.'));
			}

			$data['table']  = $this->generateTable->generate($data['table']);
		}
		return view('stock/timah/list_transaction', $data);
	}
}
