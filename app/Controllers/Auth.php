<?php namespace App\Controllers;

use App\Models\UserModel;
use App\Controllers\BaseController;

class Auth extends BaseController
{

	protected $session = null;
	protected $request = null;
	public $um;
	public $Exception;
	public $client;

	public function __construct()
	{
		$this->um 		= new UserModel();
		$this->session 	= session();
		$this->request 	= \Config\Services::request();
	}

	public function login(){
		if($this->request->getPost()){
			$data 	= $this->request->getPost();
			$login 	= $this->um->check_login($data);
			if($login->id !== null){
				// print_r($login);


				$data = [
					'id' 		=> $login->id,
					'id_role' 	=> $login->id_role,
					'role' 		=> $login->role,
					'name' 		=> $login->name,
					'email' 	=> $login->email,
				];
				$this->session->set('user', $data);
				return redirect()->to(base_url('/dashboard'));
			}else{
				$this->session->setFlashdata('msg', 'Username & Password dit not match');
				return redirect()->to(base_url('/main/login'));
			}
		}
	}


	public function logout(){
		$this->session->destroy();
		return redirect()->to(base_url('/main/login'));
	}


	//--------------------------------------------------------------------

}
