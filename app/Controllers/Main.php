<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Controllers\BaseController;

class Main extends BaseController
{
	protected $primaryKey = 'user_id';
	protected $helpers = ['form', 'date'];
	protected $session = null;
	protected $request = null;
	public $um;
	public $Exception;
	public $client;

	public function __construct()
	{
		$this->um 		= new UserModel();
		$this->session 	= session();
		$this->request 	= \Config\Services::request();
	}

	public function index(){
		if (isset($this->session->user)) {
			return redirect()->to(base_url('dashboard'));
		} else {
			return redirect()->to(base_url('main/login'));
		}
	}

	public function login(){
		return view('login');
	}

	public function logout(){
		$this->session->destroy();
		return redirect()->to(base_url('/main/login'));
	}


	//--------------------------------------------------------------------

}
