<?php 
namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\Request;
use App\Models\LoanModel;
helper('number');

class Loan extends BaseController
{
	public $lm;
	public $generateTable;
	public $filters = [
        'local_number'	=> '\CodeIgniter\View\Filters::local_number',
        'capitalize'	=> '\CodeIgniter\View\Filters::capitalize',
	];


	public function __construct()
	{
		$this->lm 		= new LoanModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
	}

	public function index()
	{
		setlocale(LC_MONETARY, 'en_US');

		$data['data']   	= $this->lm->get_data();
		$data['piutang'] 	= $this->lm->getTotalLoan();
		// print_r($data);die;
		if(count($data) > 0){
			foreach ($data['data'] as $key => $value){

				$color = ($value->type == 'in')? 'green':'red';
				$icon = ($value->type == 'in')? '<i class="fas fa-angle-down"></i>':'<i class="fas fa-angle-up"></i>';
				$data['table'][$key] = array('<a href="'.base_url('/loan/detail?name='.$value->description).'">'.$value->description.'</a>', date_format(date_create($value->date),"d M Y"),($value->type == 'in')? 'Uang Masuk' : 'Uang Keluar', '<p style="color:'.$color.'">'.$icon.'&nbsp;Rp. '. number_format($value->total,2,',','.').'</p>');
			}

			$data['table']  = $this->generateTable->generate($data['table']);
		}
		
		return view('loan/list', $data);
	}

	public function detail(){
		setlocale(LC_MONETARY, 'en_US');

		$data['data']   	= $this->lm->get_data($_GET['name']);
		$data['piutang']	= $this->lm->getTotalLoan($_GET['name']);

		if(count($data) > 0){
			foreach ($data['data'] as $key => $value){
				$color = ($value->type == 'in')? 'green':'red';
				$icon = ($value->type == 'in')? '<i class="fas fa-angle-down"></i>':'<i class="fas fa-angle-up"></i>';
				$data['table'][$key] = array($value->description, date_format(date_create($value->date),"d M Y"),($value->type == 'in')? 'Uang Masuk' : 'Uang Keluar', '<p style="color:'.$color.'">'.$icon.'&nbsp;Rp. '. number_format($value->total,2,',','.').'</p>', '<div class="mt-4 mb-2">
				<a href="'.base_url('/loan/delete?id='.$value->id."&name=".$value->description) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
			</div>');
			}
			// <div class="mt-4 mb-2">
			// 	<a href="'.base_url('/admin/master/user/edit/'.$value->id) . '" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-pen text-white"></i> </a>
			// 	<a href="'.base_url('/admin/master/user/delete/'.$value->id) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
			// </div>

			$data['table']  = $this->generateTable->generate($data['table']);
		}
		
		return view('loan/list', $data);
	}

	public function insert(){

		if (isset($_POST['date'])) {
			# code...
// 			print_r($_POST);die;
			$data = $this->lm->save($_POST);

			if($data)
				return redirect()->to(base_url('loan/detail?name='.$_POST['description']));
		}

		return view('loan/form');
	}
	
	public function delete(){

		$data = $this->lm->delete($_GET['id']);
		return redirect()->to(base_url('loan/detail?name='.$_GET['name']));

	}
}
