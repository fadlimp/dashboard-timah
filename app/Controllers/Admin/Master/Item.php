<?php

namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\Request;
use App\Models\RoleModel;
use App\Models\ItemModel;

class Item extends BaseController
{

	public $request;
	public $rm; //stands for role Model
	public $um; //stands for role Model
	public $generateTable;

	public function __construct()
	{
		$request 				= \Config\Services::request();
		$this->im				= new ItemModel();
		$this->generateTable	= new \CodeIgniter\View\Table();
	}

	public function index()
	{
		$data   = $this->im->get_data();
		// print_r($data);die;
		if (count($data) > 0) {
			foreach ($data as $key => $value)
				$data['table'][$key] = array($value->name, $value->price, $value->stock . $value->unit, '
						<div class="mt-4 mb-2">
							<a href="' . base_url('/admin/master/item/edit/' . $value->id) . '" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-pen text-white"></i> </a>
							<a href="' . base_url('/admin/master/item/delete/' . $value->id) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
						</div>');

			$data['table']  = $this->generateTable->generate($data['table']);
		}
		return view('stock/item/list', $data);
	}

	public function insert()
	{
		$data					= $this->request->getPost();
		// $data['option_role']	= $this->rm->get_data_option();

		if ($data) {
			// save to db
			$save  = $this->im->create($this->request->getPost());

			if ($save) {
				return redirect()->to(base_url('admin/master/item'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		} else {
			return view('stock/item/form', $data);
		}
	}

	public function delete($id)
	{
		$data = [
			'id' => $id,
			'delete' => '1'
		];

		$this->um->save($data);

		return redirect()->to(base_url('admin/master/item'))->with('success', 'item successfully deleted.');
	}

	function list($id = null)
	{
		$data   = $this->im->get_data($id);
		echo json_encode($data);
	}
	//--------------------------------------------------------------------

}
