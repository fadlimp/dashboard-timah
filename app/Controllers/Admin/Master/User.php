<?php 
namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\Request;
use App\Models\RoleModel;
use App\Models\UserModel;

class User extends BaseController
{

	public $request;
	public $rm; //stands for role Model
	public $um; //stands for role Model
	public $generateTable;

	public function __construct(){
		$request 				= \Config\Services::request();
		$this->rm				= new RoleModel();
		$this->um				= new UserModel();
		$this->generateTable	= new \CodeIgniter\View\Table();

	}

	public function index(){
		$data   = $this->um->get_data();
		// print_r($data);die;
		if(count($data) > 0){
			foreach ($data as $key => $value)
				$data['table'][$key] = array($value->name, $value->email, $value->role, '
						<div class="mt-4 mb-2">
							<a href="'.base_url('/admin/master/user/edit/'.$value->id) . '" class="btn btn-warning btn-circle btn-sm"><i class="fas fa-pen text-white"></i> </a>
							<a href="'.base_url('/admin/master/user/delete/'.$value->id) . '" class="btn btn-danger btn-circle btn-sm" onclick="return confirm(\'Apakah akan menghapus data ?\');"><i class="fas fa-trash text-white"></i></a>
						</div>');

			$data['table']  = $this->generateTable->generate($data['table']);
		}

		return view('admin/master/user/list', $data);
	}

	public function create(){
		$data					= null;
		$data['option_role']	= $this->rm->get_data_option();

		if(($this->request->getPost())){
			//save to db
			$save  = $this->um->create($this->request->getPost());

			if ($save) {
				return redirect()->to(base_url('admin/master/user'))->with('success', 'Data successfully saved.');
			} else {
				return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
			}
		}else{
			return view('admin/master/user/form', $data);
		}
	}

	public function delete($id)
    {
        $data = [
            'id' => $id,
            'delete' => '1'
        ];

        $this->um->save($data);

        return redirect()->to(base_url('admin/master/user'))->with('success', 'User successfully deleted.');
    }

    public function edit($id = '')
    {
            $data = [
                'option_role' => $this->rm->get_data_option(),
                'isEdit' => 'edit',
                'getUser' => $this->um->get_data($id),
                'role' => $this->rm->get_data()
            ];
            // print_r($this->rm->get_data_option());die;
            return view('admin/master/user/edit', $data);
    }

    public function save_update()
    {
    	$id_role 	= $this->request->getVar("id_role");
    	$id 	    = $this->request->getVar("id");
    	$name 		= $this->request->getVar("name");
    	$email		= $this->request->getVar("email");
    	$position	= $this->request->getVar("position");
    	$username	= $this->request->getVar("email");
    	$time		= date('Y-m-d H:i:s');

        $data = [
                'id_role' => $id_role,
                'id' => $id,
                'name' => $name,
                'email' => $email,
                'position' => $position,
                'edit_date' => $time
            ];

        $this->um->save($data);
        // print_r($data);die;
        $data_ = [
        		'id_user' => $id,
        		'username' => $username,
        		'is_active' => 1
        ];

        $this->um->update_user_username($data_);
        // print_r($data_);die;
        return redirect()->to(base_url('admin/master/user'))->with('success', 'Data user successfully Updated.');
    }
	//--------------------------------------------------------------------

}
