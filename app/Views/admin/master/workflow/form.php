<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Workflow</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - Document Workflow</h6>
		</div>
		<div class="card-body row">
			<div class="col-lg-7">
				<form method="POST" action="" class="user" accept-charset="utf-8">
					<div class="form-group">
						<label for="">Document Matrix / Workflow</label>
						<input name="name" type="text" class="form-control" placeholder="Document Matrix/Workflow" required>
					</div>

					<!-- <div class="form-group">
						<textarea name="description" class="form-control" placeholder="Deskripsi" ></textarea>
					</div> -->
					<!-- <?= $list_role;?> -->
					<div class=" form-group">
						<label for="">Approver Document Workflow</label>
						<input id="approval_tagator" type="text" name="approval" class="tagator form-control"
						 placeholder="Document Approval Workflow"
						 data-tagator-show-all-options-on-focus="true"
						 placeholder="Document Approval Workflow" required>
					</div>

					<div class="form-button-sec">
						<hr>
						<!-- Back button -->
						<a href="<?= base_url('admin/master/workflow') ?>" class="btn btn-secondary btn-icon-split">
							<span class="icon text-white-50">
								<i class="fas fa-arrow-left"></i>
							</span>
							<span class="text">Cancel</span>
						</a>
						<!-- Save Button -->
						<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
							<span class="icon text-white-50">
								<i class="fas fa-save"></i>
							</span>
							<span class="text">Save Data</span>
						</button>
					</div>

				</form>
			</div>

			<div class="col-lg-5">
				<img class="form-bg" src="<?= base_url('assets/img/bg/setup_workflow.png');?>">
			</div>
		</div>
	</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>
	<script>
		$( document ).ready(function() {
			// TAGATOR
			let list_role = <?= $list_role;?>;
			$('#approval_tagator').tagator('autocomplete', list_role);
		});
	</script>
<?= $this->endSection() ?> ?>