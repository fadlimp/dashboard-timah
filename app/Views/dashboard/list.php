<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<?php if (session()->getFlashdata('success')) { ?>
    <div class="alert alert-success">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('success')) ?>
        </div>
    </div>
<?php } ?>


          <!-- Page Heading -->

            <h1 class="h3 mb-2 text-gray-800">Dashboard</h1>
            <p class="mb-4">Laporan semua data di aplikasi dalam bentuk grafik. hubungi developer anda untuk mendapatkan akses ini. <a target="_blank" href="https://web.whatsapp.com/send?phone=6285715353287&text=Hai+Boleh+bantu+saya+buatkan+grafik?">Hubungi via whatsapp</a>.</p>

          <!-- Content Row -->
          <div class="row">

            <div class="col-xl-8 col-lg-7">

              <!-- Area Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Area Chart</h6>
                </div>
                <div class="card-body">
                  <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                  </div>
                  <hr>
                </div>
              </div>

              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Bar Chart</h6>
                </div>
                <div class="card-body">
                  <div class="chart-bar">
                    <canvas id="myBarChart"></canvas>
                  </div>
                  <hr>
                </div>
              </div>

            </div>

            <!-- Donut Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Donut Chart</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4">
                    <canvas id="myPieChart"></canvas>
                  </div>
                  <hr>
                </div>
              </div>
            </div>
          </div>
<?= $this->endSection() ?> ?>

<?= $this->section('script') ?>
<script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>

<script src="<?php echo base_url('assets/js/demo/chart-area-demo.js')?>"></script>
<script src="<?php echo base_url('assets/js/demo/chart-pie-demo.js')?>"></script>
<script src="<?php echo base_url('assets/js/demo/chart-bar-demo.js')?>"></script>
<?= $this->endSection() ?> ?>