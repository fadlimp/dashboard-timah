<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Stock Timah</h1>
</div>

<?php if (session()->getFlashdata('error')) {
?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - Transaksi Timah</h6>
	</div>
	<div class="card-body row">
		<div class="col-lg-7">
			<form method="post" autocomplete="off" action="" class="user" accept-charset="utf-8">

				<div class="form-group">
					<label for="">Tanggal</label>
					<input name="date" autocomplete="off" type="date" class="form-control" placeholder="Rp. 0" required>
				</div>

				<div class="form-group">
					<label for="">Kualitas (OC) Rata-Rata</label>
					<input name="quality_avg" type="number" class="quality_avg form-control" placeholder="OC < 60 Rendah || OC > 60 Tinggi" value="0" required>
				</div>

				<div class="form-group">
					<label for="">Total Berat (Kg)</label>
					<input name="quantity_total" type="number" class="quantity_total form-control" placeholder="OC < 60 Rendah || OC > 60 Tinggi" value="0" readonly>
				</div>

				<div class="form-group">
					<label for="">Total Harga Jual (Rp)</label>
					<input name="amount" type="number" class="form-control" placeholder="Total Harga Jual" required>
				</div>

				<div class="form-group-wrapper" id="detail-list">

					<h6 class="m-0 font-weight-bold text-primary">Detail Timah</h6>
					<div class="m-1 d-sm-flex align-items-end justify-content-end">
						<a href="#detail-list" id="addItem"><i class="fas fa-plus fa-sm text-black-50"></i> detail timah</a>
					</div>
					<!-- <hr> -->

					<div class="form-group-detail">

						<div class="form-group">

							<?= form_dropdown('stock[]', $list_timah, null, 'class="form-control timah_options" required'); ?>

							<input name="quantity[]" type="number" class="quantity form-control" placeholder="Jumlah Berat (Kg)" required>
							<p>test</p>
							<a class="removeItem" onclick="removeItem()">
								<i class="fas fa-times fa-sm text-black-50"></i>
							</a>
						</div>
					</div>
				</div>

				<div class="form-button-sec">
					<hr>
					<!-- Back button -->
					<a href="<?= base_url('admin/master/user') ?>" class="btn btn-secondary btn-icon-split">
						<span class="icon text-white-50">
							<i class="fas fa-arrow-left"></i>
						</span>
						<span class="text">Cancel</span>
					</a>
					<!-- Save Button -->
					<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
						<span class="icon text-white-50">
							<i class="fas fa-save"></i>
						</span>
						<span class="text">Save Data</span>
					</button>
				</div>

			</form>
		</div>

		<div class="col-lg-5">
			<img class="form-bg" src="<?= base_url('assets/img/bg/create-user.png'); ?>">
		</div>
	</div>
</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>
<script type="text/javascript">
	$(document).ready(function() {
		let dropdown = `<?= form_dropdown('stock[]', $list_timah, null, 'class="form-control timah_options" required'); ?>`;
		dropdown = dropdown.replace(/(\r\n|\n|\r)/gm, "")
		const inputField = '<div class="form-group">' + dropdown + '<input name="quantity[]" type="number" class="quantity form-control" placeholder="Jumlah Berat (Kg)" required><p>test</p><a class="removeItem" onclick="removeItem()"><i class="fas fa-times fa-sm text-black-50"></i></a></div>';
		$('#addItem').click(function() {
			$('.form-group-detail').append(inputField);
		})



	})

	$(document).on('click', '.removeItem', function() {
		$(this).parent().remove();
	})

	$(document).on('change', '.quantity', function() {
		var sum = 0;
		$('.quantity').each(function() {
			sum += +parseInt(this.value);
		})

		$('.quantity_total').val(sum);
	})

	$(document).on('change', '.timah_options', function() {
		var numItems = $('.timah_options').length;
		var sum = 0;
		// var val = $('option:selected', this).html();
		// val = val.split(' - ');
		// val = val[1].replace(/[^0-9\.]/g, '');


		$('.timah_options').each(function() {

			var val = $('option:selected', this).html();
			val = val.split(' - ');
			val = val[1].replace(/[^0-9\.]/g, '');

			sum += +parseInt(val);
		})

		sum = sum / numItems;
		$('.quality_avg').val(sum);
	})
</script>
<?= $this->endSection('script') ?> ?>