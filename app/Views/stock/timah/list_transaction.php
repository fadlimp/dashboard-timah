<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Transaksi Timah</h1>
    <a href="<?= base_url('/stock/timah/transaction') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mr-4"><i class="fas fa-plus fa-sm text-white-50"></i> Timah Keluar</a>

</div>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Transaksi Timah</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Jumlah (Kg)</th>
                        <th>Kualitas Rata-Rata(OC)</th>
                        <th>Total Beli</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Tanggal</th>
                        <th>Jumlah (Kg)</th>
                        <th>Kualitas Rata-Rata(OC)</th>
                        <th>Total Beli</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?= ($table); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->endSection() ?> ?>

<?= $this->section('script') ?>
<script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
<?= $this->endSection() ?> ?>