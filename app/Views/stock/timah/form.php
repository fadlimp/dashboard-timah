<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Manajemen Stok Timah</h1>
</div>

<?php if (session()->getFlashdata('error')) {
	?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - Manajemen Stok Timah</h6>
		</div>
		<div class="card-body row">
			<div class="col-lg-7">
				<form method="post" autocomplete="off" action="" class="user" accept-charset="utf-8">
					<div class="form-group">
					<label for="">Gudang</label>
						<select name="stored_at" class="form-control" required>
							<option selected>--Pilih Tempat Penyimpanan--</option>
							<option value="gudang1">Gudang 1</option>
						</select>
						<!-- <?= form_dropdown('id_role', array(''), null, 'class="form-control" required');?> -->
					</div>
					<div class="form-group">
                    	<label for="">Tanggal Masuk</label>
						<input name="date" autocomplete="off" type="date" class="form-control" placeholder="Tanggal Timah Masuk" required>
					</div>

					<div class="form-group">
                    	<label for="">Kualitas Timah (OC)</label>
						<input name="quality" step=".01" type="number" class="form-control" placeholder="OC" required>
					</div>
					<div class="form-group">
                    	<label for="">Jumlah Timah (KG)</label>
						<input name="quantity" step=".01" type="number" class="form-control" placeholder="Jumlah timah dalam Kg" required>
					</div>
					<div class="form-group">
                    	<label for="">Harga Timah (Rp/Kg)</label>
						<input name="price" step=".01" type="number" class="form-control" placeholder="Harga timah perkilo" required>
					</div>
					<div class="form-group">
                    	<label for="">Total Harga Timah</label>
						<input name="total_price" step=".01" type="number" class="form-control" placeholder="Total Harga">
					</div>

					<div class="form-group">
                    <label for="">Deskripsi/Keterangan</label>
						<textarea name="description" placeholder="Keterangan Timah" class="form-control"></textarea>
					</div>

					<div class="form-button-sec">
						<hr>
						<!-- Back button -->
						<a href="<?= base_url('admin/master/user') ?>" class="btn btn-secondary btn-icon-split">
							<span class="icon text-white-50">
								<i class="fas fa-arrow-left"></i>
							</span>
							<span class="text">Cancel</span>
						</a>
						<!-- Save Button -->
						<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
							<span class="icon text-white-50">
								<i class="fas fa-save"></i>
							</span>
							<span class="text">Save Data</span>
						</button>
					</div>

				</form>
			</div>

			<div class="col-lg-5">
				<img class="form-bg" src="<?= base_url('assets/img/bg/create-user.png');?>">
			</div>
		</div>
	</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>

<?= $this->endSection() ?> ?>