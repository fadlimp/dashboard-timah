<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between">
    <h1 class="h3 mb-0 text-gray-800">Stok Timah</h1>
</div>

<div class="d-sm-flex align-items-end justify-content-end mb-4">
    <a href="<?= base_url('/stock/timah/list_transaction') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mr-4"><i class="fas fa-flow fa-sm text-white-50"></i> Transaksi Timah</a>
    <a href="<?= base_url('/stock/timah/insert') ?>" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm mr-4"><i class="fas fa-plus fa-sm text-white-50"></i> Timah Masuk</a>
    <a href="<?= base_url('/stock/timah/transaction') ?>" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm mr-4"><i class="fas fa-plus fa-sm text-white-50"></i> Timah Keluar</a>
</div>


<?php if (session()->getFlashdata('success')) { ?>
    <div class="alert alert-success">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('success')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Stok Timah</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="20%">Deskripsi</th>
                        <th>Tanggal Beli</th>
                        <!-- <th>Gudang</th> -->
                        <th>Kualitas</th>
                        <th>Harga Beli</th>
                        <th>Jumlah Beli (Kg)</th>
                        <th width="70px">Terjual</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th width="20%">Deskripsi</th>
                        <th>Tanggal Beli</th>
                        <!-- <th>Gudang</th> -->
                        <th>Kualitas</th>
                        <th>Harga Beli</th>
                        <th>Jumlah Beli (Kg)</th>
                        <th>Terjual</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?= ($table); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->endSection() ?> ?>

<?= $this->section('script') ?>
<script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
<?= $this->endSection() ?> ?>