<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Cashflow Toko</h1>
    <a href="<?= base_url('/stock/store/insert') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data</a>
</div>
<div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Income (Total)</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $piutang;?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-money-bill fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>


<?php if (session()->getFlashdata('success')) { ?>
    <div class="alert alert-success">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('success')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Cashflow Toko</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th width="20%">Name</th>
                        <th>Tanggal</th>
                        <th>Jenis Uang</th>
                        <th>Jumlah</th>
                        <!-- <th width="70px">Action</th> -->
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th width="20%">Name</th>
                        <th>Tanggal</th>
                        <th>Jenis Uang</th>
                        <th>Jumlah</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </tfoot>
                <tbody>
                    <?= ($table); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->endSection() ?> ?>

<?= $this->section('script') ?>
<script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>
<?= $this->endSection() ?> ?>