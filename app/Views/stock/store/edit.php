<?php echo $this->extend('template/dashboard'); ?>

<?php echo $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">User</h1>
</div>

<?php if (session()->getFlashdata('error')) { ?>
    <div class="card mb-4 py-3 border-left-danger">
        <div class="card-body">
            <?php print_r(session()->getFlashdata('error')) ?>
        </div>
    </div>
<?php } ?>

<!-- DataTales Example -->

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Form - Edit User </h6>
    </div>
    <div class="card-body row">
        <div class="col-lg-7">
            <form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data" action="<?php echo base_url('admin/master/user/save_update') ?>">
                <input name="id" type="hidden" class="form-control form-control-user" id="id" placeholder="Full Name" value="<?= $getUser->id; ?>">
                    <!-- <div class="form-group">
                        <?= form_dropdown('id_role', $option_role, null, 'class="form-control" value="<?= $getUser->id_role; ?>" required');?>
                    </div> -->

                    <div class="form-group">
                    <label for="">Role</label>
                        <select class="form-control" id="id_role" name="id_role">
                            <option value="">----- Select User Role  -----</option>
                            <?php foreach ($role as $key => $value) : ?>
                                <option value="<?= $value->id; ?>" <?= ($getUser->id_role == $value->id) ? 'selected' : '' ?>><?= $value->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                    <label for="">Nama User</label>
                        <input name="name" type="text" class="form-control" value="<?= $getUser->name; ?>" placeholder="Full Name" required>
                    </div>

                    <div class="form-group">
                    <label for="">Email</label>
                        <input name="email" autocomplete="off" type="email" class="form-control" value="<?= $getUser->email; ?>" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                    <label for="">Posisi</label>
                        <input name="position" autocomplete="off" type="text" class="form-control" value="<?= $getUser->position; ?>" placeholder="Position Detail">
                    </div>
                <hr>
                <div class="form-button-sec">
                    <hr>
                    <!-- Back button -->
                    <a href="<?= base_url('admin/master/user') ?>" class="btn btn-secondary btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Cancel</span>
                    </a>
                    <!-- Save Button -->
                    <button type="Save Data" class="btn btn-primary btn-icon-split float-right">
                        <span class="icon text-white-50">
                            <i class="fas fa-save"></i>
                        </span>
                        <span class="text">Save Data</span>
                    </button>
                </div>
            </form>
        </div>
        <div class="col-lg-5">
            <img class="form-bg" src="<?= base_url('assets/img/bg/create-user.png');?>">
        </div>
    </div>
</div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
<script type="text/javascript">
    $(document).ready(function() {});
</script>
<?php echo $this->endSection() ?> ?>