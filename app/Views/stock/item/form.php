<?= $this->extend('template/dashboard'); ?>

<?= $this->section('content') ?>
<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Daftar Barang</h1>
</div>

<?php if (session()->getFlashdata('error')) {
?>
	<div class="card mb-4 py-3 border-left-danger">
		<div class="card-body">
			<?php print_r(session()->getFlashdata('error')) ?>
		</div>
	</div>
<?php } ?>

<!-- DataTales Example -->
<!-- <div class="col-lg-7"> -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form - Tambah Data Barang</h6>
	</div>
	<div class="card-body row">
		<div class="col-lg-7">
			<form method="post" autocomplete="off" action="" class="user" accept-charset="utf-8">
				<div class="form-group">
					<label for="">Toko</label>
					<select name="type" class="form-control" required>
						<option selected>--Pilih Toko--</option>
						<option value="1">Toko 1</option>
						<option value="2">Toko 2</option>

					</select>
					<!-- <?= form_dropdown('id_role', array(''), null, 'class="form-control" required'); ?> -->
				</div>

				<div class="form-group">
					<label for="">Nama Barang</label>
					<input name="name" type="text" class="form-control" placeholder="Nama" required>
				</div>

				<div class="form-group">
					<label for="">Harga Satuan</label>
					<input name="price" min="100.00" step="0.01" type="number" class="form-control" placeholder="Rp. 0" required>
				</div>

				<div class="form-group">
					<label for="">Jumlah Stock</label>
					<input name="stock" autocomplete="off" type="number" class="form-control" placeholder="1x" required>
				</div>

				<div class="form-group">
					<label for="">Satuan Unit</label>
					<select name="type" class="form-control" required>
						<option selected>--Pilih Satuan Unit--</option>
						<option value="ton">Ton</option>
						<option value="kilogram">Kilogram</option>
						<option value="liter">Liter</option>
						<option value="pcs">Pcs</option>
						<option value="cubic">Kubik</option>

					</select>
				</div>

				<div class="form-group">
					<label for="">Deskripsi Barang</label>
					<textarea name="description" type="text" class="form-control" placeholder="Deskripsi barang"></textarea>
				</div>

				<div class="form-button-sec">
					<hr>
					<!-- Back button -->
					<a href="<?= base_url('admin/master/user') ?>" class="btn btn-secondary btn-icon-split">
						<span class="icon text-white-50">
							<i class="fas fa-arrow-left"></i>
						</span>
						<span class="text">Cancel</span>
					</a>
					<!-- Save Button -->
					<button type="Save Data" class="btn btn-primary btn-icon-split float-right">
						<span class="icon text-white-50">
							<i class="fas fa-save"></i>
						</span>
						<span class="text">Save Data</span>
					</button>
				</div>

			</form>
		</div>

		<div class="col-lg-5">
			<img class="form-bg" src="<?= base_url('assets/img/bg/create-user.png'); ?>">
		</div>
	</div>
</div>
<!-- </div> -->

<?= $this->endSection() ?> ?>
<?= $this->section('script') ?>

<?= $this->endSection() ?> ?>