<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url('assets/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url('assets/css/sb-admin-2.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/css/animate.min.css'); ?>" rel="stylesheet">
  <!-- <link href="<?php #echo base_url('assets/css/hashtag.css'); ?>" rel="stylesheet"> -->

  <!-- Custom styles for tables page -->
  <link href="<?= base_url('assets/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/tagator/fm.tagator.jquery.css') ?>" rel="stylesheet" type="text/css">

  <!-- Images Slides -->
  <link href="https://www.w3schools.com/w3css/4/w3.css">


  <!-- Page level custom css -->
  <?= $this->renderSection('css'); ?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

	<!-- Sidebar -->
	<?php include 'sidebar.php' ?>
	<!-- End of Sidebar -->

	<!-- Content Wrapper -->
	<div id="content-wrapper" class="d-flex flex-column">

	  <!-- Main Content -->
	  <div id="content">

		<!-- Topbar -->
		<?php include 'navbar.php'; ?>
		<!-- End of Topbar -->

		<!-- Begin Page Content -->
		<div class="container-fluid">
			<?= $this->renderSection('breadcrumb'); ?>
			<?= $this->renderSection('content'); ?>
		</div>
		<!-- /.container-fluid -->

	  </div>
	  <!-- End of Main Content -->

	  <!-- Footer -->
	  <footer class="sticky-footer bg-white">
		<div class="container my-auto">
		  <div class="copyright text-center my-auto">
			<span>Copyright &copy; Worldbank eLibrary 2021</span>
		  </div>
		</div>
	  </footer>
	  <!-- End of Footer -->

	</div>
	<!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
		  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		  </button>
		</div>
		<div class="modal-body">Yakin Akan Melakukan Logout ?</div>
		<div class="modal-footer">
		  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
		  <a class="btn btn-primary" href="<?= base_url('main/logout'); ?>">Logout</a>
		</div>
	  </div>
	</div>
  </div>

	<script>
		var base_url = "<?= base_url()?>";
	</script>
	<!-- Bootstrap core JavaScript-->
	<script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/contextMenu.js'); ?>"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?= base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?= base_url('assets/js/sb-admin-2.min.js'); ?>"></script>
	<script src="<?= base_url('assets/vendor/chart.js/Chart.min.js'); ?>"></script>

	<!-- Page level plugins -->
	<script src="<?= base_url('assets/vendor/datatables/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>

	<!-- Tagator autocomplete tags plugins -->
	<script src="<?= base_url('assets/vendor/tagator/fm.tagator.jquery.js') ?>"></script>

	<!-- Page level plugins -->
	<!-- Page level custom script -->
	<?= $this->renderSection('script'); ?>

	<ul class="contextMenu"></ul>

	<div class="customization_popup detail_popup is_visible" role="alert">
		<div class="customization_popup_container">
			
			<a href="#0" class="customization_popup_close img-replace"><i style="color: red" class="fa fa-close"></i></a>
			<p id="details_title">Judul bos</p>
			<div class="tabs">
				<input type="radio" name="tabs" id="tabone" checked="checked">
				<label for="tabone">Details</label>
				<div class="tab" id="details_tab">
					<table>
						<tr>
							<th>Type</th>
							<td>isi</td>
						</tr>
						<tr>
							<th>Owner</th>
							<td>isi</td>
						</tr>
						<tr>
							<th>Created</th>
							<td>isi</td>
						</tr>
					</table>
				</div>
				
				<input type="radio" name="tabs" id="tabtwo">
				<label for="tabtwo">Activity</label>
				<div class="tab"  id="activity_tab">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
		</div> 
	</div>
	
	<script>
		$(document).ready(function(){
			$('.form-bg').attr('draggable', false);

			$('.customization_popup').on('click', function(event) {
				if ($(event.target).is('.customization_popup_close') || $(event.target).is('.customization_popup')) {
					event.preventDefault();
					$(this).removeClass('is-visible');
				}
			});

			$(document).keyup(function(event) {
				if (event.which == '27') {
					$('.customization_popup').removeClass('is-visible');
				}
			});

		});
		
	</script>

</body>

</html>