<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-cloud-upload-alt"></i>
		</div>
		<div class="sidebar-brand-text mx-3">eLibrary <sup>wb</sup></div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<!-- <li class="nav-item active">
		<a class="nav-link upload-file" href="<?= base_url('doc/create') ?>">
		<i class="fas fa-file-upload"></i>
		<span>Upload File</span></a>
	</li> -->

	<!-- Nav Item - Charts -->
	<li class="nav-item active">
		<a class="nav-link" href="<?= base_url('dashboard'); ?>">
			<i class="fas fa-chart-line"></i>
			<span>Dashboard</span></a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<div class="sidebar-heading">
		Stock Management
	</div>

	<!-- Nav Item - Charts -->
	<li class="nav-item">
		<a class="nav-link" href="<?= base_url('stock/store'); ?>">
			<i class="fas fa-store"></i>
			<span>Toko</span></a>
	</li>

	<!-- Nav Item - Charts -->
	<li class="nav-item">
		<a class="nav-link" href="<?= base_url('stock/timah'); ?>">
			<i class="fas fa-coins"></i>
			<span>Timah</span></a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">



	<!-- Heading -->
	<div class="sidebar-heading">
		Cash Flow
	</div>
	<!-- Nav Item - Charts -->
	<li class="nav-item">
		<a class="nav-link" href="<?= base_url('loan'); ?>">
			<i class="fas fa-cash-register"></i>
			<span>Hutang Piutang</span></a>
	</li>
	<!-- <li class="nav-item">
		<a class="nav-link" href="<?= base_url('timah'); ?>">
			<i class="fas fa-coins"></i>
			<span>Pembelian Timah</span></a>
	</li> -->

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<i class="fas fa-fw fa-database"></i>
			<span>Master</span>
		</a>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Data Master</h6>
				<a class="collapse-item" href="<?= base_url('admin/master/item'); ?>">Daftar Barang</a>
				<a class="collapse-item" href="<?= base_url('admin/master/user'); ?>">Daftar Pelanggan</a>
			</div>
		</div>
	</li>
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>