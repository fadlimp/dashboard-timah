<?php

namespace CodeIgniter\View;

use CodeIgniter\Database\BaseResult;

/**
 * HTML Table Generating Class
 *
 * Lets you create tables manually or from database result objects, or arrays.
 *
 * @package    CodeIgniter
 * @subpackage Libraries
 * @category   HTML Tables
 * @author     EllisLab Dev Team
 */
class File
{

	/**
	 * Data for table rows
	 *
	 * @var array
	 */
	public $rows = [];

	/**
	 * Data for table heading
	 *
	 * @var array
	 */
	public $heading = [];

	/**
	 * Data for table footing
	 *
	 * @var array
	 */
	public $footing = [];

	/**
	 * Whether or not to automatically create the table header
	 *
	 * @var boolean
	 */
	public $autoHeading = true;

	/**
	 * Table caption
	 *
	 * @var string
	 */
	public $caption;

	/**
	 * Table layout template
	 *
	 * @var array
	 */
	public $template;

	/**
	 * Newline setting
	 *
	 * @var string
	 */
	public $newline = "\n";

	/**
	 * Contents of empty cells
	 *
	 * @var string
	 */
	public $emptyCells = '';

	/**
	 * Callback for custom table layout
	 *
	 * @var function
	 */
	public $function;

	/**
	 * Set the template from the table config file if it exists
	 *
	 * @param  array $config (default: array())
	 * @return void
	 */
	public function __construct($config = [])
	{
		// initialize config
		foreach ($config as $key => $val)
		{
			$this->template[$key] = $val;
		}
	}

	/**
	 * Set "empty" cells
	 *
	 * Can be passed as an array or discreet params
	 *
	 * @param  mixed $value
	 * @return Table
	 */
	public function setEmpty($value)
	{
		$this->emptyCells = $value;
		return $this;
	}

	/**
	 * Prep Args
	 *
	 * Ensures a standard associative array format for all cell data
	 *
	 * @param  array
	 * @return array
	 */

	// --------------------------------------------------------------------

	/**
	 * Generate the table
	 *
	 * @param  mixed $fileData
	 * @return string
	 */
	public function generateFile($fileData = null)
	{
		
		$display = null;
		if(!empty($fileData)){
			foreach ($fileData as $key => $value) {
				# code...
				$display .= $this->_fileTemplate($value->id, $value);
			}
		}else{
			$display = '<div class="data-not-found" style="
							width: 100%;
							text-align: center;
							line-height: 40vh;
						">
							<p>No data available</p>
						</div>';
		}

		return $display;
	}

	public function generateFolder($fileData = null)
	{
		// print_r($fileData);
		$display = null;
		if(!empty($fileData)){
			foreach ($fileData as $key => $value) {
				# code...
				$display .= $this->_folderTemplate($value);
			}
		}else{
			$display = '<div class="data-not-found" style="
							width: 100%;
							text-align: center;]
							line-height: 40vh;
						">
							<p>No data available</p>
						</div>';
		}

		return $display;
	}

	public function generateBreadcrumb(){
		$request 	= \Config\Services::request();
		$uri 		= $request->uri->getPath();
		$q 			= explode('=', $request->uri->getQuery());

		$display = $this->_breadcrumbTemplate($q[1]);

		return $display;
	}
	// --------------------------------------------------------------------

	/**
	 * Default Template
	 *
	 * @return array
	 */
	protected function _fileTemplate($url = '#', $data)
	{
		
		$data			= (is_array($data))? (object)$data : $data;
		$file_type		= pathinfo($data->doc_file);
		$custom_icon	= null;

		switch ($file_type['extension']) {
			case 'xls':
				$custom_icon = 'assets/img/icon/excel.png';
				break;
			case 'xlsx':
				$custom_icon = 'assets/img/icon/excel.png';
				break;
			case 'ppt':
				$custom_icon = 'assets/img/icon/powerpoint.png';
				break;
			case 'pptx':
				$custom_icon = 'assets/img/icon/powerpoint.png';
				break;
			case 'doc':
				$custom_icon = 'assets/img/icon/word.png';
				break;
			case 'docx':
				$custom_icon = 'assets/img/icon/word.png';
				break;
			default:
				$custom_icon = 'assets/img/icon/file.png';
				break;
		}
		
		$style			= 'style="background:center no-repeat url('.base_url($custom_icon).'); background-size: 100px;"';

		$html = '<!-- FOLDER AREA -->
				<div class="col-lg-2">
					<div class="file-box" id='.$data->id.'>
						<a>
							<div class="file-icon" '.$style.'>&nbsp;</div>
							<div class="file-name card-header">
								'.$data->name.'
							</div>
						</a>
						<div class="file-menu">
							<ul>
								<li>
									<a href="#" class="menu-more-trigger" id="menu-trigger-'.$data->id.'">
										<i class="fas fa-ellipsis-h"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END OF FOLDER-->';
		return $html;
	}

	protected function _folderTemplate($data)
	{

		$data = (array) $data;
		$html = '<!-- Folder -->
				<div class="file-list col-lg-12" id='.$data['id'].'>
					<a href="'.$data['url'].'">
						<div class="file-icon">&nbsp;</div>
						<div class="file-name card-header">
							'.$data['name'].'
						</div>
					</a>
				</div>';
		return $html;
	}

	protected function _breadcrumbTemplate($path){
		$html = '<div class="breadcrumb">';
		$html .= '<a href="'.base_url().'">/</a>';
		$html .= '<a href="'.base_url('doc/category').'">Category</a>';
		$html .= '<a href="#">'.$path.'</a>';
		$html .= '</div>';

		return $html;
	}

	protected function generateFileIcon($file){
		$ext	= '';
		$_ext	= null;

		switch ($ext) {
			case 'css':
				$_ext = 'Cascading Style Sheet file';
				break;
			case 'xls':
				$_ext = 'Microsoft Excel file';
				break;
			case 'xlsx':
				$_ext = 'Microsoft Excel Open XML spreadsheet file';
				break;
			case 'ppt':
				$_ext = 'PowerPoint  presentation';
				break;
			case 'pptx':
				$_ext = 'PowerPoint Open XML presentation';
				break;
			case 'doc':
				$_ext = 'Microsoft Word File';
				break;
			case 'docx':
				$_ext = 'Microsoft Word File';
				break;
			case 'pdf':
				$_ext = 'PDF File';
				break;
			case 'txt':
				$_ext = 'Plain text file';
				break;
			default:
				$_ext = ext;
				break;
		}
		return $_ext;
	}

	// --------------------------------------------------------------------
}
